# Pack The Fittest

Client

## How to Run

>>>
1. Clone project
  * git clone https://gitlab.com/VEnriquez89010/pack-the-fittest-client.git

2. Install and Run Server-Api
  * [https://gitlab.com/VEnriquez89010/pack-the-fittest-server](https://gitlab.com/VEnriquez89010/pack-the-fittest-server)

3. Run project
  * cd pack-the-fittest-client
  * npm install
  * npm start


4. Listen on port 3000, examples
  * Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
>>>


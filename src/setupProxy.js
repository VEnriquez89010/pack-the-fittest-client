var proxy = require('http-proxy-middleware');
let SERVER_URL = 'http://localhost:4000';

module.exports = function(app){
  app.use(
    proxy('/api', { 
      target: SERVER_URL, 
      changeOrigin: true ,
      secure: true
    })
  );
};

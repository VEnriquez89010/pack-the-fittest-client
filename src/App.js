import React, { Component } from 'react';
import './App.scss';
import { Route, Switch } from 'react-router-dom';
import { Content } from 'carbon-components-react/lib/components/UIShell';
import Header from './components/Header';
import Sidebar from './components/Sidebar';
import Home from './content/Home';
import Profile from './content/Profile';
import Signup from './content/Signup';
import Login from './content/Login';
import ForgotPass from './content/ForgotPass';
import NewPass from './content/NewPass';
import Redirect from './content/Redirect';
import Logout from './content/Logout';
import PrivacyPolicy from './content/PrivacyPolicy';
import TermsAndConditions from './content/TermsAndConditions';


class App extends Component {
  render() {
    return (
      <>
        <Content>
        <Sidebar></Sidebar>
        <Header></Header>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/profile" component={Profile} />
            <Route path="/signup" component={Signup} />
            <Route path="/login" component={Login} />
            <Route path="/forgotpass" component={ForgotPass} />
            <Route path="/newpass" component={NewPass} />
            <Route path="/redirect" component={Redirect} />
            <Route path="/logout" component={Logout} />
            <Route path="/privacy-policy" component={PrivacyPolicy} />
            <Route path="/terms-and-conditions" component={TermsAndConditions} />
          </Switch>
        </Content>

      </>
    );
  }
}

export default App;

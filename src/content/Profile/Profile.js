import React from 'react';
import Footer from '../../components/Footer';
import ls from 'local-storage'
import { Button  } from 'carbon-components-react';
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import { getToken } from '../../helpers/SessionControl';
import SortableComponent from '../../components/DragDropContent';


class Profile extends React.Component {
   /*
  constructor(props) {
    super(props);

    let session = ls.get('token');
   
    if (!session) {
      window.location.href='/login';
      return;
    }

    this.state = {
      name: ls.get('name'),
      data: []
    };
  }

  componentDidMount() {
    this.getInfo();
  }

  getInfo(){
      axios.get(`${appSettings.SERVER_URL}/users/all`, getToken())
      .then(users => {
        console.log(users)
      }).catch(error => error + '');
  } */


  render() {
    return (
              <>
              <div className="profileStyle">
                <div className="bx--grid">
                  <div className="bx--row">
                  <div className="bx--col-md-2"></div>
                  <div className="bx--col-md-4">
                      <div className="centerTitle">
                          <p>Hola</p>
                          <h3>{/*this.state.name*/}</h3>
                      </div>
                      <br></br>
                      <br></br>
                      <div className="profileContainer">

                        <SortableComponent />

                        <Button type="submit" value="Submit" className="buttonAccess" onClick={()=> window.location.href='/calendar'}>
                        Save
                        </Button>




                      </div>
                  </div>
                  <div className="bx--col-md-2"></div>
                  </div>
                  
                </div>
              </div>
              <Footer />
              </>
            );
};
}

export default Profile;

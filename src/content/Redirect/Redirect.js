import React from 'react';
import axios from 'axios';
import ls from 'local-storage'
import appSettings from '../../helpers/AppSettings';

class NameForm extends React.Component {
  constructor(props) {
    super(props);
    
    let id = props.location.pathname.split('/').pop();

    axios.get(`${appSettings.SERVER_URL}/users/external-login/${id}`)
      .then(res => {
            if(res.status !== 404){
                ls.set('session', res.data.user.Id);
                ls.set('name', res.data.user.Name);
                ls.set('token', res.data.token);
                window.location.href='/profile';
            }else{
              window.location.href='/';
            }   
      }).catch(function (error) { window.location.href='/'; });
  }
  
  render() {
      return (<p>Redirecting...</p>)
  }
}

export default NameForm;
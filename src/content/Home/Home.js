import React from 'react';
import './Home.scss?v=2.5.0';
import IconFacebook from './icon-facebook.svg';
import IconInstagram from './icon-instagram.svg';
import { Link } from 'react-router-dom';
import { Button } from 'carbon-components-react';
import { ChevronRight32, Cyclist32 } from '@carbon/icons-react';
import Footer from '../../components/Footer';
import ls from 'local-storage'


const renderAuthButton = ()=>{
  let session = ls.get('session');
  if(session == null){
    return  <Link element={Link} to="/signup">
              <Button renderIcon={Cyclist32}>Start now</Button>
            </Link>
  } else {
    return <Link element={Link} to="/">
              <Button renderIcon={Cyclist32}>Start playing</Button>
           </Link>
  }
}

const Home = () => {
  return (
    <>
    <div className="heroHome">
      <div className="bx--grid bx--grid--full-width landing-page">
        <div className="heroHeadline">

          <div className="alignBottom" data-type='home'>
            <h1 >CrossFit is a branded fitness regimen created by Greg Glassman.</h1>
            <p>It is a registered trademark of CrossFit, Inc.</p>
            <br></br>
            <br></br>
            <br></br>
            {renderAuthButton()}

            <ul className="socialNetworks" data-type='heroHome'>
                  <li>
                    <a href="https://www.instagram.com">
                      <img src={IconInstagram} alt='' />  
                    </a>
                  </li>
                  <li>
                    <a href="https://facebook.com">
                      <img src={IconFacebook} alt='' />  
                    </a>
                  </li>
                </ul>
          </div>

        </div>

          <div className="bx--row">

          <div className="bx--col-md-12 bx--col-lg-12">

          </div>
          </div>
    </div>

    </div>
    
    <div className="aboutHome">
    <div className="bx--grid">
    <div className="bx--row">
        <div className="bx--col-lg-8">
           <h1>Lorem.</h1>
            <br></br>
            <br></br>
            <p>
          
            </p>
            <br></br>
            <br></br>
            <Link element={Link} to="/studio">
              <Button renderIcon={ChevronRight32}>Conoce el mas</Button>
            </Link>
            <br></br>

          </div>
          <div className="bx--col-lg-8">
          </div>

        </div>
    </div>
    </div>
    <Footer />
    </>
  );
};
export default Home;

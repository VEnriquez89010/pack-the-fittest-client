import React, {Component} from 'react';
import './DragDrop.scss';
import {SortableContainer, SortableElement} from 'react-sortable-hoc';
import arrayMove from 'array-move';
import logo from './logo.png';

const SortableItem = SortableElement(({value}) => <li>{value}</li>
    );

const SortableList = SortableContainer(({items}) => {
  return (
    <ul className="DragDropList">
      {items.map((value, index) => (
        <SortableItem key={`item-${value}`} index={index} value={value} />
      ))}
    </ul>
  );
});

class SortableComponent extends Component {
  state = {
    items: [
            'Mattew Fraser', 
            'Patrick Vellner', 
            'Noah Ohlsen', 
            'Brent Fikowski', 
            'Ben Smith', 
            'Mikko Salo'
        ],
  };
  onSortEnd = ({oldIndex, newIndex}) => {
    this.setState(({items}) => ({
      items: arrayMove(items, oldIndex, newIndex),
    }));
  };
  render() {
    return <SortableList items={this.state.items} onSortEnd={this.onSortEnd} />;
  }
}
  
export default SortableComponent;
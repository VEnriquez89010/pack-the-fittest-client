import React from "react";
import './Sidebar.scss';
import Logo from './logo.png';
import Sidebar from "react-sidebar";
import { Link } from 'react-router-dom';
import ls from 'local-storage'



class SidebarMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sidebarOpen: false,
    };
    this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
  }

  onSetSidebarOpen(open) {
    document.body.style.overflow =  (open) ? "hidden" : "";
    this.setState({ sidebarOpen: open });
  }
  

  render() {
    const renderAuthButton = ()=>{
      let session = ls.get('session');
      if(session == null){
        return <Link element={Link} to="/login">Iniciar sesión</Link> 
      } else{
        return <Link element={Link} to="/logout">Cerrar sesión</Link> 
      }
    }

    return (
      <Sidebar
        sidebar={
            <div className="SidebarContainer">
            <ul className="Sidebar">
                <Link element={Link} to="/">
                    <img src={Logo} alt='Pack the fittest' className="SidebarLogo" />
                </Link>
                <li><Link element={Link} to="/">Link 1</Link></li>
                <li><Link element={Link} to="/">Link 2</Link></li>
                <li><Link element={Link} to="/">Link 3</Link></li>
                <li><Link element={Link} to="/">Link 4</Link></li>
                <div className="alignBottom" data-type='sideBar'>
                <li className="rooedaSidebarLoginControl">
                  {renderAuthButton()}
                </li>
            </div>
            </ul>

            </div>
        }
        open={this.state.sidebarOpen}
        onSetOpen={this.onSetSidebarOpen}
        shadow={false}
        styles={{ 
            root: { },
            sidebar: { 
                position: "absolute",
                background: "rgba(10,10,10, 0.60)", 
                backdropFilter: "saturate(180%) blur(20px)",
                WebkitBackdropFilter: "saturate(180%) blur(20px)",
                paddingTop: "25pt",
                paddingLeft: "0pt",
                width: "25wv",
                zIndex: 99999,
                transition: "transform .2s ease-out",
                WebkitTransition: "-webkit-transform .2s ease-out"
            },
            content: { overflowY: 'auto' },
            overlay: {
                backgroundColor: "rgba(25,25,25, 0.30)",
                overflow: "hidden",
                zIndex: 999,
                transition: "opacity .1s ease-out, visibility .1s ease-out",
            }
        }}
        >
        <div className="burguerMenu" 
             onClick={() => this.onSetSidebarOpen(true)} 
        >
        </div>
      </Sidebar>
    );
  }
}

export default SidebarMenu;

import React from 'react';
import { Link } from 'react-router-dom';

const Agreement = () => (

    <div className="alignBottom" data-type='privacyPolicy'>
    <div className="privacyPolicy">By continuing, you agree to the Pack the fittest's 
        <Link element={Link} to="/terms-and-conditions" className="privacyPolicy">
          Terms and conditions
        </Link>
          and the
        <Link element={Link} to="/privacy-policy" className="privacyPolicy">
          Privacy policy.
        </Link>
    </div>
    </div>

);

export default Agreement;
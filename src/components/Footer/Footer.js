import React from 'react';
import './Footer.scss?v=2.0.0';
import logo from './logo.png';
import IconLocked from './icon-locked.svg';
import IconFacebook from './icon-facebook.svg';
import IconInstagram from './icon-instagram.svg';
import { Link } from 'react-router-dom';
import { Button } from 'carbon-components-react';
import { Cyclist32 } from '@carbon/icons-react';
import ls from 'local-storage'

const renderAuthButton = ()=>{
  let session = ls.get('session');
  if(session == null){
    return <Link element={Link} to="/login">Iniciar sesión</Link> 
  } else{
    return <Link element={Link} to="/logout">Cerrar sesión</Link> 
  }
}

const footerAuthButton = ()=>{
  let session = ls.get('session');
  if(session == null){
    return <Link element={Link} to="/signup"><Button renderIcon={Cyclist32}>Regístrate ahora</Button></Link>
  } else{
    return <Link element={Link} to="/"><Button renderIcon={Cyclist32}>Ir a una page</Button></Link>
  }
}


const Footer = () => {
    return (

      <>
      <div className="footerStyle">
        <div className="bx--grid bx--grid--full-width landing-page">
          <div className="bx--row ">
            <div className="bx--col-md-4 bx--col-lg-4">
                <img src={logo} alt='website logo' className="footerIcon" />
                <p>
                  CrossFit is a branded fitness regimen created by Greg Glassman.
                </p>
                <br></br>
                <br></br>
                {footerAuthButton()}
            </div>

            <div className="bx--col-md-4 bx--col-lg-4">
                <ul className="footerMenu">
                  <li><Link element={Link} to="/studio">Link 1</Link></li>
                  <li><Link element={Link} to="/instructors">Link 2</Link></li>
                  <li><Link element={Link} to="/packs">Link 3</Link></li>
                  <li><Link element={Link} to="/calendar">Link 4</Link></li>
                  <li>{renderAuthButton()}</li>
                </ul>
            </div>
          
            <div className="bx--col-md-4 bx--col-lg-4 footerContact">
                <h3>hola@mail.com</h3>
                <br></br>
                <p>
                  <strong>Lunes</strong> a <strong>Sábado</strong> de 6 AM - 9 PM
                </p>
                <br></br>
                <p className="footerAddress">
                  <strong>Nombre</strong> Direccion
                  <br></br>
                  Ciudad, municipio
                </p>
                <ul className="socialNetworks">
                  <li>
                    <a href="https://www.instagram.com">
                      <img src={IconInstagram} alt='' />  
                    </a>
                  </li>
                  <li>
                    <a href="https://facebook.com">
                      <img src={IconFacebook} alt='' />  
                    </a>
                  </li>
                </ul>
            </div>
            </div>

            <div className="bx--row footerRooeda">
              <div className="bx--col-md-6 bx--col-lg-6">
                  <img src={IconLocked} alt='Rooeda Studio' className="footerIconLocked" />
                  <p>Protegido con SSL <span>|</span>{new Date().getFullYear()} Pack the fittest™</p>
              </div>
              <div className="bx--col-md-6 bx--col-lg-6">
                  <Link element={Link} to="/terms-and-conditions">
                    Condiciones de uso
                  </Link>
              </div>
            </div>

        </div>
      </div>
      </>
    );
  };
  export default Footer;